from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests, json

# Create your views here.
response = {}


def show_profile(request):
    print('session-id' in request.session)
    if 'session-id' in request.session:
        company_data = get_company_data(request)
        print(company_data)
        response['companyName'] = company_data.get('name', '')
        response['companyType'] = company_data.get('companyType', {}).get('name', '')
        response['companyWeb'] = company_data.get('websiteUrl', '')
        specialties = ", ".join(company_data.get('specialties',{}).get('values',''))
        response['companySpecialty'] = specialties
        response['squareLogoUrl'] = company_data.get('squareLogoUrl', '')
        response['companyDescription'] = company_data.get('description', '')
        response['flag'] = True;
    else:
        response['flag'] = False;
        return redirect('/')
    return render(request, 'compProfile.html', response)

def get_company_id(request):
    token = request.session['session-id']
    url = "https://api.linkedin.com/v1/companies?oauth2_access_token="+token+"&format=json&is-company-admin=true"
    company_id = requests.request("GET", url=url)
    company_id = json.loads(company_id.content.decode('utf8'))
    print(company_id['values'])
    company_id = str(company_id['values'][-1]['id'])
    print(company_id)
    return company_id

def get_company_data(request):
    token = request.session['session-id']
    cpy_id = request.session['company_id']
    url = "https://api.linkedin.com/v1/companies/"+cpy_id+":(id,name,ticker,description,company-type,website-url,specialties,square-logo-url)?oauth2_access_token="+token+"&format=json"
    company_data = requests.request("GET", url=url)
    company_data = json.loads(company_data.content.decode('utf8'))
    return company_data
