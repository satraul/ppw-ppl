#  from django.shortcuts import render
from fitur1.models import Company
from fitur3.models import Forum, Comment
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from django.http import JsonResponse, HttpRequest
import json

# Create your views here.
response = {}
# def index(request):
#     return render(request, 'index.html', response)

@csrf_exempt
def add_comment(request: HttpRequest):
    response = {}
    if request.method == "POST":
        decoded_request = request.body.decode("utf-8")
        print(decoded_request)
        data = json.loads(decoded_request)
        id      = data.get('id', None)
        name    = data.get('name', None)
        content = data.get('content', None)

        status = 0
        if (id == None or name == None or content == None):
            status = 400
            response.update({
                'error': 'you need to include the id, content and name',
            })
        if not Forum.objects.filter(id=id).exists():
            status = 404
            response.update({
                'error': 'requested id:' + str(id) + ' does not exists',
            })
        else:

            company_id = request.session['company_id']
            forum = Forum.objects.get(id=id)
            company = forum.company

            from_company = (company_id == company.companyID)

            latest_comment = Comment.objects.create(name=name, content=content, forum=forum, from_company=from_company)

            status = 200
            data = model_to_dict(latest_comment)
            if from_company:
                data.update({"img_url":company.companyImage,
                             "company_id":company.companyID,
                             "company_name":company.companyName})

            response.update({
                'status':'success',
                'data':data,
            })

        return JsonResponse(response, status=status,
                            content_type="application/json")

@csrf_exempt
def get_comment(request: HttpRequest):
    if request.method == "GET":
        id = request.GET.get('id', None)
        if id != None:
            forum = Forum.objects.filter(id=id).latest('id')
            company = forum.company
            comments = list(Comment.objects.filter(forum=forum).values())
            for i in comments:
                i.update({"img_url":company.companyImage,
                          "company_id":company.companyID,
                          "company_name":company.companyName})
            return JsonResponse(comments, status=200, safe=False,
                                content_type="application/json")
        else:
            return JsonResponse(
                { 'error': 'requested id:' + str(id) + ' does not exists',},
                status=400, content_type="application/json"
            )
