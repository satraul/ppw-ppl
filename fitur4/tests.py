from django.test import TestCase, Client
from unittest import skip
#  from django.urls import resolve
from fitur3.models import Comment, Forum
#  from unittest import skip
import json

# Create your tests here.
class CommentTest(TestCase):
    @skip("nanti")
    def test_add_comment_reject_get(self):
        response = Client().get('/fitur4/add_comment/')
        self.assertEqual(response.status_code, 400)

    @skip("nanti")
    def test_add_comment_reject_invalid_id(self):
        name    = "some name"
        content = "this is a content"
        session = self.client.session
        session['company_id'] = '13598320'
        session.save()

        response = self.client.post('/fitur4/add_comment/',
                                    data=json.dumps({
                                        'id':-1,
                                        'name':name,
                                        'content':content
                                    }),
                                    content_type="application/json")
        self.assertEqual(response.status_code, 404)

    def test_get_comment_reject_get(self):
        response = Client().get('/fitur4/get_comment/')
        self.assertEqual(response.status_code, 400)

    @skip("nanti")
    def test_comment_model_can_be_created(self):
        name    = "some name"
        content = "this is a content"
        forum   = Forum.objects.create(content=content)
        Comment.objects.create(name=name, content=content, forum=forum)

        comment_object_count = Comment.objects.all().count()
        self.assertEqual(comment_object_count, 1)

    @skip("nanti")
    def test_add_comment_can_POST(self):
        session = self.client.session
        session['company_id'] = '13598320'
        session.save()

        name    = "some name"
        content = "this is a content"
        Forum.objects.create(content=content)
        response = self.client.post('/fitur4/add_comment/',
                                    data=json.dumps({
                                        'id':1,
                                        'name':name,
                                        'content':content
                                    }),
                                    content_type="application/json")
        self.assertEqual(response.status_code, 200)

        comment_object_count = Comment.objects.all().count()
        self.assertEqual(comment_object_count, 1)
        self.assertIn('success', response.content.decode('utf8'))

    def test_add_comment_POST_failed(self):
        pass

    @skip("nanti")
    def test_get_comment_can_GET(self):
        session = self.client.session
        session['company_id'] = '13598320'
        session.save()

        name    = "some name"
        content = "this is a content"
        Forum.objects.create(content=content)
        self.client.post('/fitur4/add_comment/',
                                    data=json.dumps({
                                        'id':1,
                                        'name':name,
                                        'content':content
                                    }),
                                    content_type="application/json")
        response = self.client.get('/fitur4/get_comment/?id=1')
        comments = response.content.decode('utf8')
        self.assertIn(name, comments)
        self.assertIn(content, comments)

    def test_get_comment_GET_failed(self):
        pass
