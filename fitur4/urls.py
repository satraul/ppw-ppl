from django.conf.urls import url
from .views import add_comment, get_comment
#url for app


urlpatterns = [
    url(r'^add_comment/$', add_comment, name='add_comment'),
    url(r'^get_comment/$', get_comment, name='get_comment'),
]
