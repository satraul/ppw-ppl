from django.db import models
from fitur1.models import Company

# Create your models here.
class Forum(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey(Company)
    content = models.TextField(max_length=500)

    class Meta:
       ordering = ['id']

class Comment(models.Model):
    name    = models.CharField(max_length=140)
    from_company = models.BooleanField(default=False)
    #  title   = models.CharField(max_length=140)
    content = models.CharField(max_length=500)
    forum   = models.ForeignKey(Forum)
    created_on = models.DateTimeField(auto_now_add=True)
