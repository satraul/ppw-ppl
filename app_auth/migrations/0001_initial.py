# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-01-26 13:54
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, to=settings.AUTH_USER_MODEL)),
                ('npm', models.CharField(editable=False, max_length=10, primary_key=True, serialize=False, unique=True, verbose_name='NPM')),
                ('role', models.CharField(blank=True, max_length=10, verbose_name='Role')),
                ('angkatan', models.PositiveIntegerField(verbose_name='Angkatan')),
                ('is_showing_score', models.BooleanField(default=False)),
                ('picture_url', models.CharField(blank=True, max_length=300)),
                ('id_linkedin', models.CharField(blank=True, max_length=20)),
                ('link_linkedin', models.CharField(blank=True, max_length=100)),
                ('lastseen_at', models.DateTimeField(auto_now=True, verbose_name='Last Seen at')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
            ],
            options={
                'ordering': ('npm', 'first_name', 'last_name', 'created_at', 'lastseen_at'),
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
