"""ppw_ppl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include, static
from django.contrib import admin
from django.views.generic.base import RedirectView
import fitur1.urls as fitur1
import fitur2.urls as fitur2
import fitur3.urls as forum
import fitur4.urls as fitur4

urlpatterns = [
    url(r'^', include('app_web.urls', namespace='web')),
    #url(r'^b/', RedirectView.as_view(url='login/', permanent = True), name = '$'),
    url(r'^admin/', admin.site.urls),
    # Paket A
    url(r'^auth/', include('app_auth.urls', namespace='auth')),
    url(r'^api/friend/', include('app_friend.urls', namespace='api-friend')),
    url(r'^api/profile/', include('app_profile.urls', namespace='api-profile')),
    url(r'^api/service/', include('app_service.urls', namespace='api-service')),
    url(r'^api/status/', include('app_status.urls', namespace='api-status')),
    # Paket B
    url(r'^login/', include(fitur1, namespace='login')),
    url(r'^profile/', include(fitur2, namespace='fitur2')),
    url(r'^forum/', include(forum, namespace='forum')),
    url(r'^fitur4/', include(fitur4, namespace='fitur4')),
] + static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
  + static.static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)