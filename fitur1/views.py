from django.shortcuts import render, redirect
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect
from django.urls import reverse
from fitur3.models import Forum, Comment
from fitur1.models import Company
from fitur3.forms import ForumForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from fitur2.views import get_company_id
from helper.utils import *
import requests, json

CLIENT_ID = "866hc1so0n9js9"
CLIENT_SECRET = "IFSny4QeaB0XYT8D"
REDIRECT_URI = "http://127.0.0.1:8000/login/request-token/"

# Create your views here.
response = {}
def index(request):
    forum_list = Forum.objects.all()
    paginator = Paginator(forum_list, 3)

    page = request.GET.get('page')
    try:
        forum = paginator.page(page)
    except:
        forum = paginator.page(1)

    response['forum'] = forum
    if 'session-id' in request.session:
        response['flag'] = True;
        data_profile = get_profile_data(request)
        print(data_profile)
        response['nama'] = (data_profile['firstName'] if 'firstName' in data_profile else "") + " " + (data_profile["lastName"] if 'lastName' in data_profile else "")
    else:
        response['flag'] = False;
    return render(request, 'test.html', response)
