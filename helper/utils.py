from django.shortcuts import render, redirect
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect
from django.urls import reverse
from fitur1.models import Company
from fitur2.views import get_company_id
from helper.utils import *
import requests, json

CLIENT_ID = "866hc1so0n9js9"
CLIENT_SECRET = "IFSny4QeaB0XYT8D"
REDIRECT_URI = "http://tp2-uyay.herokuapp.com/login/request-token/"
response = {}

def request_auth(request: HttpRequest):
    uri = ("https://www.linkedin.com/oauth/v2/authorization?" +
           "response_type=code&" +
           "client_id=" + CLIENT_ID + "&" +
           "redirect_uri=" + REDIRECT_URI + "&" +
           "state=987654321&" +
           "scope=r_basicprofile r_emailaddress rw_company_admin w_share")
    return redirect(uri)

def request_token(request: HttpRequest):
    request_token_uri = "https://www.linkedin.com/oauth/v2/accessToken"
    print(1)

    if request.method == "GET":
        code = request.GET['code']
        print("code = " + code)
        state = request.GET['state']
        request_data = {
            "grant_type":"authorization_code",
            "code":code,
            "redirect_uri":REDIRECT_URI,
            "client_id":CLIENT_ID,
            "client_secret":CLIENT_SECRET
        }

        request_header = {
            'content_type':"application/json"
        }
        response = requests.request("POST", request_token_uri, data=request_data,
                         headers=request_header)
        response_data = json.loads(response.content.decode('utf8'))
        request.session['session-id'] = response_data['access_token']
        #  return HttpResponseRedirect('/profile/', reques)
        new_company_id = get_company_id(request)
        request.session['company_id'] = new_company_id
        company_data = get_company_data(request)
        is_exist = Company.objects.filter(companyID=new_company_id)
        if is_exist.count() == 0:
            specialties = ", ".join(company_data.get('specialties',{}).get('values',''))
            Company.objects.create(companyName=company_data.get('name', ''),
            companyType=company_data.get('companyType', {}).get('name', ''),
            companyID=new_company_id,
            companyWeb=company_data.get('websiteUrl', ''),
            companySpecialty=specialties,
            companyImage=company_data.get('squareLogoUrl', ''),
            companyDescription=company_data.get('description', ''))

        return redirect('/profile/')
    else:
        return redirect('/')

def auth_logout(request):
    print(2)
    request.session.flush() # menghapus semua session
    response.clear()
    return HttpResponseRedirect(reverse('login:index'))

def get_profile_data(request):
    print(3)
    token = request.session['session-id']
    url = "https://api.linkedin.com/v1/people/~?oauth2_access_token="+token+"&format=json"
    data_profile = requests.request("GET", url=url)
    data_profile = json.loads(data_profile.content.decode('utf8'))
    print(data_profile)
    return data_profile


def show_profile(request):
    print(4)
    print('session-id' in request.session)
    if 'session-id' in request.session:
        company_data = get_company_data(request)
        print(company_data)
        response['companyName'] = company_data.get('name', '')
        response['companyType'] = company_data.get('companyType', {}).get('name', '')
        response['companyWeb'] = company_data.get('websiteUrl', '')
        specialties = ", ".join(company_data.get('specialties',{}).get('values',''))
        response['companySpecialty'] = specialties
        response['squareLogoUrl'] = company_data.get('squareLogoUrl', '')
        response['companyDescription'] = company_data.get('description', '')
        response['flag'] = True;
    else:
        response['flag'] = False;
        return redirect('/')
    return render(request, 'compProfile.html', response)

def get_company_id(request):
    print(5)
    token = request.session['session-id']
    url = "https://api.linkedin.com/v1/companies?oauth2_access_token="+token+"&format=json&is-company-admin=true"
    company_id = requests.request("GET", url=url)
    company_id = json.loads(company_id.content.decode('utf8'))
    size = len(company_id['values'])
    company_id = str(company_id['values'][size-1]['id'])
    print(company_id)
    return company_id

def get_company_data(request):
    print(6)
    token = request.session['session-id']
    cpy_id = request.session['company_id']
    url = "https://api.linkedin.com/v1/companies/"+cpy_id+":(id,name,ticker,description,company-type,website-url,specialties,square-logo-url)?oauth2_access_token="+token+"&format=json"
    company_data = requests.request("GET", url=url)
    company_data = json.loads(company_data.content.decode('utf8'))
    return company_data
